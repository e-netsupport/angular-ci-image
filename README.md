# Angular CI Image

Docker-Image für Angular CLI zur Verwendung als Build-Container und zur ausführung von Tests.


## Example usage
```
docker run -u $(id -u) --rm -v "$PWD":/app registry.gitlab.com/e-netsupport/angular-ci-image:latest ng new MyDemo
cd MyDemo
docker run -u $(id -u) --rm -v "$PWD":/app registry.gitlab.com/e-netsupport/angular-ci-image:latest ng build
```

Um den Angular CLI-Entwicklungsserver über Docker auszuführen, muss der Port zugeordnet werden und die Angular CLI muss auf alle Schnittstellen hören.
Zum Beispiel:
```
cd MyDemo
docker run -u $(id -u) --rm -p 4200:4200 -v "$PWD":/app registry.gitlab.com/e-netsupport/angular-ci-image:latest ng serve --host 0.0.0.0
```

Docker-Container zum Ausführen von Karma-Tests mit Angular CLI in Docker

## Vorbereitungen für Tests

Vor beginn des Testens sollte die Datei karma.conf.js um folgende Profile erweitert werden und der SingleRun auf true geändert werden:

```javascript
    customLaunchers: {
      HeadlessChrome: {
        base: "ChromeHeadless",
        flags: [
          "--headless",
          "--no-sandbox",
          "--disable-gpu",
          "--remote-debugging-port-9222"
        ]
      },
      HeadlessFirefox: {
        base: 'Firefox',
        flags: [
          "-headless"
        ],
        prefs: {
          'network.proxy.type': 0,
          'media.navigator.permission.disabled': true
        }
      }
    },
    singleRun: true,
```

## Ausführen von Karma-Unit-Tests im Docker-Container

```
docker run -u $(id -u) --rm -v "$PWD":/app registry.gitlab.com/e-netsupport/angular-ci-image:latest ng test
```

## Ausführen von Karma-Unit-Tests im Docker-Container, Beenden nach dem Testlauf
```
docker run -u $(id -u) --rm -v "$PWD":/app registry.gitlab.com/e-netsupport/angular-ci-image:latest ng test --watch false --single-run true
```